#!/bin/bash
#Copyright inria / irisa (2013)
#
#
#pierre.peterlongo@inria.fr
#
#This software is a computer program whose purpose is performe optimized targeted assembly from reads
#
#This software is governed by the CeCILL license under French law and
#abiding by the rules of distribution of free software.  You can  use,
#modify and/ or redistribute the software under the terms of the CeCILL
#license as circulated by CEA, CNRS and INRIA at the following URL
#"http:#www.cecill.info".
#
#As a counterpart to the access to the source code and  rights to copy,
#modify and redistribute granted by the license, users are provided only
#with a limited warranty  and the software's author,  the holder of the
#economic rights,  and the successive licensors  have only  limited
#liability.
#
#In this respect, the user's attention is drawn to the risks associated
#with loading,  using,  modifying and/or developing or reproducing the
#software by the user in light of its specific status of free software,
#that may mean  that it is complicated to manipulate,  and  that  also
#therefore means  that it is reserved for developers  and  experienced
#professionals having in-depth computer knowledge. Users are therefore
#encouraged to load and test the software's suitability as regards their
#requirements in conditions enabling the security of their systems and/or
#data to be ensured and,  more generally, to use and operate it in the
#same conditions as regards security.
#
#The fact that you are presently reading this means that you have had
#knowledge of the CeCILL license and that you accept its terms.

#######################################################################
#################### HEADER, CHANGE VALUES HERE #######################
#######################################################################
k=29
b=""
Dopt=""
#######################################################################
#################### END HEADER                 #######################
#######################################################################
function help {
echo "clean_all_tools.sh is a script cleaning for mapsembler2_extremities, mapsembler2_extend and kissreads_graph or kissreads to build mapsembler2_pipeline"
echo "Usage: ./compile_all_tools.sh "
echo "removes all executables and compiled files"
echo "Any further question: read the readme file or contact us: pierre.peterlongo@inria.fr"
}

#######################################################################
#################### GET OPTIONS                #######################
#######################################################################
while getopts "h" opt; do
case $opt in
h)
help
exit
;;
\?)
echo "Invalid option: -$OPTARG" >&2
exit 1
;;
:)
echo "Option -$OPTARG requires an argument." >&2
exit 1
;;
esac
done

rm -rf tools

echo "#######################################################################
################## CLEAN MAPSEMBLER2 EXTREMITIES ####################
#######################################################################"

cd mapsembler2_extremities
if [ -d "./build" ]
	then
rm -rf build
fi

echo "#######################################################################
#################### CLEAN MAPSEMBLER2 EXTEND #######################
#######################################################################"

if [ -d "./thirdparty/zlib/build" ]
	then
	rm -rf ./thirdparty/zlib/build/
fi

cd ../mapsembler2_extend
make clean
cd ..


echo "#######################################################################
###################### CLEAN KISSREADSGRAPH #########################
#######################################################################"

cd kissreads_graph
make clean
cd ..

echo "#######################################################################
######################## CLEAN KISSREADS ############################
#######################################################################"

cd kissreads
make clean
cd ..


echo "Cleaning is done"