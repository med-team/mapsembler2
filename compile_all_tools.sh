#!/bin/bash
#Copyright inria / irisa (2013)
#
#
#pierre.peterlongo@inria.fr
#
#This software is a computer program whose purpose is performe optimized targeted assembly from reads
#
#This software is governed by the CeCILL license under French law and
#abiding by the rules of distribution of free software.  You can  use,
#modify and/ or redistribute the software under the terms of the CeCILL
#license as circulated by CEA, CNRS and INRIA at the following URL
#"http:#www.cecill.info".
#
#As a counterpart to the access to the source code and  rights to copy,
#modify and redistribute granted by the license, users are provided only
#with a limited warranty  and the software's author,  the holder of the
#economic rights,  and the successive licensors  have only  limited
#liability.
#
#In this respect, the user's attention is drawn to the risks associated
#with loading,  using,  modifying and/or developing or reproducing the
#software by the user in light of its specific status of free software,
#that may mean  that it is complicated to manipulate,  and  that  also
#therefore means  that it is reserved for developers  and  experienced
#professionals having in-depth computer knowledge. Users are therefore
#encouraged to load and test the software's suitability as regards their
#requirements in conditions enabling the security of their systems and/or
#data to be ensured and,  more generally, to use and operate it in the
#same conditions as regards security.
#
#The fact that you are presently reading this means that you have had
#knowledge of the CeCILL license and that you accept its terms.

#######################################################################
#################### HEADER, CHANGE VALUES HERE #######################
#######################################################################
k=29
b=""
Dopt=""
#######################################################################
#################### END HEADER                 #######################
#######################################################################
function help {
echo "compile_all_tools.sh is a script compilation for mapsembler2_extremities, mapsembler2_extend and kissreads_graph or kissreads to build mapsembler2_pipeline"
echo "Usage: ./compile_all_tools.sh <options>"
echo -e "<options>:"
echo -e "\t \t -k <k value>: Define the k value with witch build mapsembler2_extend and kissreads_graph"
echo -e "\t \t -D: Download the nodeWebkit package to build Graph Sequence Viewer (GSV) in desktop app (GSVDesktop). Without this option GSV can be use only with a browser as web app"
echo -e "\t \t -b <path of boost install>: Set the default path of boost install"
echo -e "\t\t -h Prints this message and exist"
echo "Any further question: read the readme file or contact us: pierre.peterlongo@inria.fr"
}

#######################################################################
#################### GET OPTIONS                #######################
#######################################################################
while getopts "b:k:Dh" opt; do
case $opt in
k)
echo "compilation of Mapsembler2_extend and KissreadsGraph with k= $OPTARG" >&2
k=$OPTARG
;;
b)
echo "path of boost install used: $OPTARG" >&2
b=$OPTARG
;;
D):
echo "use -D: nodeWebkit will be downloaded to build GSV desktop app" >&2
Dopt="-D"
;;
h)
help
exit
;;
\?)
echo "Invalid option: -$OPTARG" >&2
exit 1
;;
:)
echo "Option -$OPTARG requires an argument." >&2
exit 1
;;
esac
done
#######################################################################
#################### END GET OPTIONS            #######################
#######################################################################
#if test $# -eq 0
#then
#	k=29
#else
#    k=$1
#fi

if [ -d "./tools" ];
then echo "./tools directory already exist"	
else 
	mkdir tools
fi

echo "#######################################################################
################## COMPILE MAPSEMBLER2 EXTREMITIES ####################
#######################################################################"

cd mapsembler2_extremities
if [ -d "./build" ];
then echo "./build directory already exist"	
else 
	mkdir build
fi
cd build

if [ "$b" == "" ] ; then
cmake ..
else
cmake -DBOOST_ROOT=$b ..
fi
make -j 

cd ../../
cp  ./mapsembler2_extremities/build/mapsembler2_extremities tools
if [ $? -ne 0 ]
then
echo "There was a problem compiling Mapsembler2_extremities, sorry. Please fix the problem or contact pierre.peterlongo@inria.fr."
exit
fi

echo "#######################################################################
#################### COMPILE MAPSEMBLER2 EXTEND #######################
#######################################################################"

if [ -d "./thirdparty" ]
	then
	cd ./thirdparty/
	if [ -d "./zlib" ]
		then
		cd ./zlib/
		if [ -d "./build" ]
			then echo "./build directory already exist"	
		else 
			mkdir build
		fi
		cd ./build
		cmake ..
		make
		cd ../../../	
	else 
		echo "./zlib directory doesn't exist"
		exit
	fi	
else 
	echo "./thirdparty directory doesn't exist"
	exit
fi

cd mapsembler2_extend
make k=$k
cd ..
cp mapsembler2_extend/mapsembler_extend tools/mapsembler2_extend
if [ $? -ne 0 ]
then
echo "There was a problem compiling Mapsembler2_extend, sorry. Please fix the problem or contact pierre.peterlongo@inria.fr."
exit
fi

echo "#######################################################################
###################### COMPILE KISSREADSGRAPH #########################
#######################################################################"

cd kissreads_graph
make k=$k
cd ..
cp kissreads_graph/kissreads_graph tools
if [ $? -ne 0 ]
then
echo "There was a problem compiling KissreadsGraph, sorry. Please fix the problem or contact pierre.peterlongo@inria.fr."
exit
fi

echo "#######################################################################
######################## COMPILE KISSREADS ############################
#######################################################################"

cd kissreads
make k=$k
cd ..
cp kissreads/kissreads tools
if [ $? -ne 0 ]
then
echo "There was a problem compiling Kissreads, sorry. Please fix the problem or contact pierre.peterlongo@inria.fr."
exit
fi

cd visu
cp GSV/doc/doc.pdf ../docs/GSV_doc.pdf
if [ "$Dopt" == "-D" ]
then
	sh buildGSV.sh
	
	cd ../
	if [ -d "./tools/GSVDesktop/" ];
    	then
       	 	rm -R tools/GSVDesktop/

	fi

	if [ -d "./visu/GSVDesktop.d/" ];
    	then 		
		mv visu/GSVDesktop.d tools/GSVDesktop/
	fi

	if [ -f "./GSVDesktop" ] || [ -d "./GSVDesktop.app" ];
    	then 
		echo "GSVDesktop shortcut already exist"
	else

		if [[ "$OSTYPE" == "linux" ]];
   			then  ln -s tools/GSVDesktop/GSVDesktop ./
		elif [[ "$OSTYPE" == "darwin"* ]];
    			then  ln -s tools/GSVDesktop/GSVDesktop.app ./
		fi
	fi
fi

if [ $? -ne 0 ]
    then
    echo "There was a problem compiling Graph Sequence Viewer, sorry. Please fix the problem or contact pierre.peterlongo@inria.fr."
exit
fi

echo "Compiling is done, if possible, :"
echo -e "\t 1/ copy executables \"tools/mapsembler2_extremities\", \"tools/mapsembler2_extend\", \"tools/kissreads_graph\", \"tools/kissreads\" and \"tools/GSVDesktop in a directory member of the PATH environment variable (e.g. /usb/local/bin)"
echo -e "\t 2/ replace PATH_RS=\"./tools\" by PATH_RS=\"\" in the \"run_mapsembler_and_phaser.sh\" configuration file"
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized
if [ -z "$MY_PATH" ] ; then
# error; for some reason, the path is not accessible
# to the script (e.g. permissions re-evaled after suid)
exit 1  # fail
fi

echo "Leave it as is. In this case, if working outside this current directory (\"$MY_PATH\"), you will have to indicate in the \"run_mapsembler_and_phaser.sh\" where executables \"mapsembler\", \"phaser\" and \"GSV\" are located by changing the value of the PATH_RS variable"
