Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mapsembler2
Upstream-Contact: pierre.peterlongo@inria.fr
Source: http://colibread.inria.fr/mapsembler2
Files-Excluded:
    visu
    results_visualization
    mapsembler2_extremities/thirdparty/gatb-core/thirdparty/boost
    mapsembler2_extremities/thirdparty/gatb-core/thirdparty/hdf5
    mapsembler2_extremities/thirdparty/gatb-core/doc/design
    mapsembler2_extremities/thirdparty/gatb-core/examples/build

Files: *
Copyright: 2013 INRIA / IRISA
License: AGPL-3+

Files: mapsembler2_extremities/thirdparty/gatb-core/*
Copyright: 2014 INRIA R.Chikhi, G.Rizk, E.Drezen
License: AGPL-3+

Files: mapsembler2_extremities/thirdparty/gatb-core/thirdparty/emphf/*
Copyright: 2013 Giuseppe Ottaviano <giuott@gmail.com>
License: Apache-2.0
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

Files: kissreads_graph/read_coherence_mapsembler/libchash.*
Copyright: 1998-2005 Google Inc.
License: BSD-3-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
      * Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the following disclaimer
  in the documentation and/or other materials provided with the
  distribution.
      * Neither the name of Google Inc. nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
 .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: minia/*
Copyright: 2012 Rayan Chiki <rayan.chikhi@ens-cachan.org>
           2012 Guillaume Rizk
License: AGPL-3+

Files: debian/*
Copyright: 2013 Olivier Sallou <osallou@debian.org>
License: GPL-2+
 The Debian packaging is licensed under the GPL which is available at
 `/usr/share/common-licenses/GPL-2'.

License: AGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 The complete text of the GNU Affero General Public License
 can be found in the file `/usr/share/doc/mapsembler2/LICENCE.md.gz'.
