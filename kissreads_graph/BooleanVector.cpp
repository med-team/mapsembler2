//
//  BooleanVector.cpp
//  kissreads_g
//
//  Created by Pierre Peterlongo on 02/01/13.
//  Copyright (c) 2013 Pierre Peterlongo. All rights reserved.
//

#include "BooleanVector.h"
#include "commons.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


BooleanVector::BooleanVector (long size){
    boolean_vector_size=size;
//    printf("init bv with size %ld\n", size);
    boolean_vector = (char*)malloc(boolean_vector_size/8+1);
    test_alloc(boolean_vector);
    memset(boolean_vector, 0, boolean_vector_size/8+1);
    mask[0]=1;  //00000001
    mask[1]=2;  //00000010
    mask[2]=4;  //00000100
    mask[3]=8;  //00001000
    mask[4]=16; //00010000
    mask[5]=32; //00100000
    mask[6]=64; //01000000
    mask[7]=128;//10000000
}


bool BooleanVector::is_boolean_vector_visited (long i){
    return (boolean_vector[i/8]&mask[i%8])==0?false:true;
}

void BooleanVector::set_boolean_vector_visited(long i){
    boolean_vector[i/8]|=mask[i%8];
}

BooleanVector::~BooleanVector(){
    free(boolean_vector);
}

void BooleanVector::reinit(){
    memset(boolean_vector, 0, boolean_vector_size/8);
}

/*int main(int argc, char **argv) {
 uint64_t size = 2296000000;
 init_boolean_vector(size);
 
 int number=0;
 while(1){
 number++;
 uint64_t r = random()%size;
 printf("%llu %d-", r,is_boolean_vector_visited(r));
 if(is_boolean_vector_visited(r)) break;
 set_boolean_vector_visited(r);
 printf("%d\n", is_boolean_vector_visited(r));
 }
 printf("\n%d done\n", number);
 return 1;
 
 
 }*/
