//
//  BooleanVector.h
//  kissreads_g
//
//  Created by Pierre Peterlongo on 02/01/13.
//  Copyright (c) 2013 Pierre Peterlongo. All rights reserved.
//

#ifndef __kissreads_g__BooleanVector__
#define __kissreads_g__BooleanVector__

#include <iostream>

class BooleanVector {
public:
    char mask [8];
    char * boolean_vector; // a entry=8 values
    long boolean_vector_size;

    BooleanVector(long size);
    ~BooleanVector();
bool is_boolean_vector_visited (long i);
void set_boolean_vector_visited(long i);
void free_boolean_vector();
void reinit();
};
#endif /* defined(__kissreads_g__BooleanVector__) */
