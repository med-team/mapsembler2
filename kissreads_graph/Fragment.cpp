#include "Fragment.h"
#include "commons.h"
extern char comp['t'+1];
Fragment::Fragment(char *fragment_sequence, char *fragment_comment) : fragment_sequence(strdup(fragment_sequence)), fragment_comment( strdup(fragment_comment))
{
  Mapped = NULL;
}

Fragment::Fragment(char *fragment_sequence) : fragment_sequence(strdup(fragment_sequence))
{
  Mapped = NULL;
  fragment_comment = strdup(">comment not provided");
}


Fragment::~Fragment(){
    if(Mapped)
        hash_delete(Mapped, list_of_generic_free);
  free(fragment_sequence);
  free(fragment_comment);
}
bool Fragment::isPalindromic(){
    if(strlen(fragment_sequence)%2) return false;
    const int size = strlen(fragment_sequence);
    for(int i=0;i<size/2;i++){
        if(fragment_sequence[i]!=comp[fragment_sequence[size-i-1]]) return false;
    }
    return true;
}


Fragment_Starting::Fragment_Starting(char *fragment_sequence) :Fragment(fragment_sequence) {}
Fragment_Starting::Fragment_Starting(char *fragment_sequence, char *fragment_header) :Fragment(fragment_sequence, fragment_header) {}
Fragment_Starting::~Fragment_Starting(){}

void Mapped_Fragment::alloc_coverage_quality(const int nb){
#ifdef CHARQUAL
coverage = (unsigned char **) malloc(sizeof(unsigned char *)*nb); test_alloc(coverage);
for(int i=0;i<nb;i++){
    coverage[i]= (unsigned char *) malloc(sizeof(unsigned char)*strlen(fragment_sequence));
    test_alloc(coverage[i]);
}
#else
coverage = (int **) malloc(sizeof(int *)*nb); test_alloc(coverage);
for(int i=0;i<nb;i++){
    coverage[i]= (int *) malloc(sizeof(int)*strlen(fragment->fragment_sequence));
    test_alloc(coverage[i]);
}
#endif
for(int i=0;i<nb;i++) for(int j=0;j<strlen(fragment_sequence);j++) coverage[i][j]=0;
}

Mapped_Fragment::Mapped_Fragment(char * fragment_sequence, const int number_of_read_sets) : Fragment(fragment_sequence) {
    
    read_coherent = (char *) malloc(sizeof(char)*number_of_read_sets);    test_alloc(read_coherent);
    sum_quality_per_position=NULL;
    number_mapped_reads=NULL;
}

Mapped_Fragment::Mapped_Fragment(Mapped_Fragment * clone) : Fragment(clone->fragment_sequence){
    read_coherent = NULL;
    sum_quality_per_position=NULL;
    number_mapped_reads=NULL;
    // IF WE ARE HERE, we duplicated a node. Thus we don't know its coverage. No need to create infos related to its quality or coverage.
    // the idea is to write back the graph in a file and to re-run the whole mapping.
    
    
}

Mapped_Fragment::~Mapped_Fragment(){
    if(read_coherent)
        free(read_coherent);
    if(sum_quality_per_position)
        free(sum_quality_per_position); // TODO: free for each read set
    if(number_mapped_reads)
        free(number_mapped_reads);
}
