//
//  Loader.h
//  kissreads_g
//
//  Created by Pierre Peterlongo on 02/01/13.
//  Copyright (c) 2013 Pierre Peterlongo. All rights reserved.
//

#ifndef kissreads_g_Loader_h
#define kissreads_g_Loader_h

#include <vector>
#include <assert.h>
#include <stdio.h>

#include "json.h"
#include "DeBruijnGraph.h"

DBG* load_json(char * json_file, int nb_sets);
vector<DBG*> load_mapsembler_json(char * json_file, int nb_sets);
#endif
