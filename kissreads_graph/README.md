## Run KissreadsGraph

KissreadsGraph maps the provided reads on the graph, for example with output graph files of Mapsembler2. It can be run alone with the command: `./phaser <input_graph> <readsC1.fasta/fastq[.gz]> [<readsC2.fasta/fastq[.gz]> [<readsC3.fasta/fastq[.gz] ...] <option> `.  

***Options available :*** 
*   `-M` the input is considered as a Mapsembler output, thus composed of multiple independent graphs.
*   `-t type`:   
    * `"coverage"` or `"c"`: outputs an equivalent graph removing uncovered edges and adding:
            - for each node: the coverage per sample and per position
            - for each edge: the number of mapped reads per sample using this edge
    * `"modify"` or `"m"`: outputs the same simplified graph:
	        - removing low covered edges and nodes (less that min_coverage)
	        - then phasing simple non branching paths
*   `-o file_name`: write obtained graph. Default: standard output.	
        Example: -o "res_phaser"
*   `-k size_seed`: will use seeds of length size_seed.
	Example: -k 25
*   `-c min_coverage`: Will consider an edge as coherent if coverage (number of reads from all sets using this edge) is at least min_coverage. 
        Example: -c 2
*	`-d max_substitutions`: Will map a read on the graph with at most max_substitutions substitutions.
        Example: -d 1

***By default mapsembler2 use :*** 
* `-o standard output` 
* `-k 25`
* `-c 2`
* `-d 1`

***Mandatory options are:***
* `-t type` : needed to define type of output file.


### License
Copyright INRIA - CeCILL License    
