//
//  ReadMapper.h
//  kissreads_g
//
//  Created by Pierre Peterlongo on 03/01/13.
//  Copyright (c) 2013 Pierre Peterlongo. All rights reserved.
//

#ifndef __kissreads_g__ReadMapper__
#define __kissreads_g__ReadMapper__

#include <iostream>
#include "DeBruijnGraph.h"
#include "commons.h"

/**
 * Map all reads (and their reverse complements) contained in files sored in the Bank.
 * TODO: Modify the graph info (edges and nodes coverage, nodes quality)
 */
uint64_t map_reads(Bank *read_bank, vector<DBG*> dbgs);
#endif /* defined(__kissreads_g__ReadMapper__) */
