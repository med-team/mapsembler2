/**
 * Copyright INRIA and ENS, contributors Peterlongo and Chikhi
 * pierre.peterlongo@inria.fr
 * rayan.chikhi@irisa.fr
 *
 * This software is a computer program whose purpose is to detect the
 * presence of a starter in a set of NGS reads, and to provide the neighbor
 * in case of success..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * read_coherence.h
 *
 *  Created on: 22 oct. 2010
 *      Author: ppeterlo
 */

#ifndef READ_COHERENCE_H_
#define READ_COHERENCE_H_

#define MAX_MAPPED_READS 100000 // max number of reads mapped to a single starter
#define MAX_GROUPS 20000 // max number of groups during computation
#define MAX_GROUPS_TO_RETURN 100 // max number of groups returned after computation

#define ACTG(s) ((s=='A')?0:( (s=='C')?1:( (s=='T')?2:3)))
char ** read_coherent_homologs(const hash_t map, const char * fragment, const int max_substitutions_allowed, const int min_coverage, const int error_threshold, const int size_seed, int *nb_consensuses);
char ** extend_generic(const hash_t map, const char * fragment, const int min_extension_coverage_depth, const int error_threshold, int direction, int * nb_consensuses);
char ** extend_right_only (const hash_t map, const char * fragment, const int min_extension_coverage_depth, const int error_threshold,  int * nb_consensuses);
char ** extend_left_only (const hash_t map, const char * fragment, const int min_extension_coverage_depth, const int error_threshold,  int * nb_consensuses);

#endif /* READ_COHERENCE_H_ */
