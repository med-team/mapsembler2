/**
 * Copyright INRIA and ENS, contributors Peterlongo and Chikhi
 * pierre.peterlongo@inria.fr
 * rayan.chikhi@irisa.fr
 *
 * This software is a computer program whose purpose is to detect the
 * presence of a starter in a set of NGS reads, and to provide the neighbor
 * in case of success..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*
 * hashing used by mapsembler
 * this is a generic file, which defines an interface between mapsembler and a hash table implementation
 */

#include <stddef.h>
#include <unistd.h>
#include <stdint.h>

#ifndef _HASH_H
#define _HASH_H
#include "simplehash.h"
#include "couple.h"
/*
 * generic types: we typecast in hash_*.c functions for specific implementations
 */

typedef unsigned int *hash_t;
typedef long* hash_iter;

/*
 * more advanced functions:
 *  - search for an element having specific value in the hash table
 *  - maintain a list inside each element of the hash table
 *  - return a list of sorted reads by position
 */
void hash_increase_int_value_if_exists(hash_t map, char * key);

int* hash_get_int_value(hash_t map, char * key);

void hash_set_int_to_key(hash_t map, char * key, int value);

extern ssize_t hash_search_key_int_value(hash_t map, const char *key, const int value_to_search);

void hash_add_int_to_list(hash_t map, char * key, int value);

void hash_add_something_to_list(hash_t map, const char * key, void *something);

void * hash_list_sorted_by_positions(hash_t map);

extern int hash_delete(hash_t map, void (*specific_free)(const void *));

void hash_clear(hash_t map, void (*specific_free)(const void *));

/*
 * optimizations
 */
ssize_t fast_hash_entry_by_key(hash_t given_map, const char *key, int k, uint64_t *fast_hash, void **data);

#endif                          /* _HASH_H */
