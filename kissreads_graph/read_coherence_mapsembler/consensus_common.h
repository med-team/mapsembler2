/**
 * Copyright INRIA and ENS, contributors Peterlongo and Chikhi
 * pierre.peterlongo@inria.fr
 * rayan.chikhi@irisa.fr
 *
 * This software is a computer program whose purpose is to detect the
 * presence of a starter in a set of NGS reads, and to provide the neighbor
 * in case of success..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#define MAX_VOTES 50000 // max length of the vote arrays
#define MAX_CONSENSUS 10000 // max number of consensuses for a search
#define MAX_LEN_EXTENTION 10000


#ifndef CONSENSUS_COMMON_H_
#define CONSENSUS_COMMON_H_

void init_votes(int votes[MAX_VOTES][4]);
void filter_votes(int votes[MAX_VOTES][4], int min_coverage);
void trim_votes(int votes[MAX_VOTES][4], int position);
void analyze_votes(int votes[MAX_VOTES][4], int error_threshold);
int read_coherence_from_votes(int votes[MAX_VOTES][4], int start, int end, int min_coverage);
void perform_consensus_with_read(char *fragment, char *read, int position, int threshold, int votes[MAX_VOTES][4], char ** consensus , int *nb_consensuses, int * consensuses_weights, char ** consensuses_coverages);
void populate_votes(char *read, int read_len, int position_read, int position_reference, int votes[MAX_VOTES][4]);
int rudimentary_error_correction( char *ext, int votes[MAX_VOTES][4], int vote_start_pos, int threshold, char *error_corrected);
int sequences_are_equals(char *a, int len_a, char *b, int len_b);
void maximal_consensus_prefix(char consensus[MAX_CONSENSUS][MAX_LEN_EXTENTION], int nb_consensuses);
int overlap_perfect(char *read1, int pos_read1, int read1_len, char *read2, int pos_read2);


#endif // CONSENSUS_COMMON_H_
