/**
 * Copyright INRIA and ENS, contributors Peterlongo and Chikhi
 * pierre.peterlongo@inria.fr
 * rayan.chikhi@irisa.fr
 *
 * This software is a computer program whose purpose is to detect the
 * presence of a starter in a set of NGS reads, and to provide the neighbor
 * in case of success..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#ifdef __cplusplus
extern "C"
{
#endif

/*
 * misc_tools.c
 *
 *  Created on: 25 oct. 2010
 *      Author: ppeterlo
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "../commons.h"
#include "couple.h"
#include "list.h" 

/**
 * add src at the end of dest (adding memory)
 * pointer dest may be modified, return it.
 */
char * right_concat (char *dest, const char *src){
	if(src == NULL) return dest;

	dest = realloc(dest, strlen(dest)+strlen(src)+1);

	test_alloc(dest);
	strcat(dest, src);
	return dest;
}

/**
 * add src at the beginning of of dest (adding memory). The old starting beginning of dest is then at the end.
 * pointer dest may be modified, return it.
 */
char * left_concat (char *dest, char *src){
	if(src == NULL) return dest;

	char * res = (char *) malloc (strlen(dest)+strlen(src)+1); test_alloc(res);
	strcpy(res, src);
	strcat(res,dest);
	free(dest);
	return res;

}



void list_of_charint_couple_print(list *l)
{
	cell *c=l->first;
	while (c != NULL)
	{
		printf("cell %s %d ",((p_charint_couple)(c->val))->a,((p_charint_couple)(c->val))->b);
		c=c->prox;
	}
	printf("\n");
}

int charint_couple_comparison(p_charint_couple a,p_charint_couple b)
{
  if (a->b != b->b)
	  return a->b > b->b;

  // if positions are equal, count the string length
  int len_a=a->a_len;
  int len_b=b->a_len;
  if (len_a!=len_b)
	  return len_a<len_b;
  // if both are equal, count number of U's

  // however (semantic optimization), there's no U if positions are >0, hence return strcmp
  if ((a->b >=0) && (b->b >=0))
	  //return strcmp(a->a,b->a); // strcmp was nice, but not practical with large sets + underlying bubble sort.
	  	  	  	  	  	  	      // besides, I think it was useful only for contained reads filtering, which i'm not relying on atm
	  return 0;


  int nbU_a=0;
  int nbU_b=0;
  int i=0;
  while (*((a->a)+i)!='\0')
  {
	  if (*((a->a)+i)=='U')
		  nbU_a++;
	  i++;
  }
  i=0;
  while (*((b->a)+i)!='\0')
  {
  	  if (*((b->a)+i)=='U')
  		  nbU_b++;
  	  i++;
  }
  if (nbU_a != nbU_b)
	  return nbU_a<nbU_b;

  // if number of U's are equal, do an old-fashioned strcmp
  //return strcmp(a->a,b->a);
  return 0;
}

/*
 * list bubble sort
 *
 * adapted from:
 * http://www.c.happycodings.com/Sorting_Searching/code5.html
 */
void list_bubble_sort_by_charint_couple(list *l) {
	cell *head=l->first;

	cell *a = NULL;
	cell *b = NULL;
	cell *c = NULL;
	cell *e = NULL;
	cell *tmp = NULL;

	if (head==NULL)
		return;

	/*
 // the `c' node precedes the `a' and `e' node
 // pointing up the node to which the comparisons
 // are being made.
	 */
	while(e != head->prox) {
		c = a = head;
		b = a->prox;
		while(a != e) {

			/*
			 * the custom comparison function: (a->data > b->data)
			 * here we know a->val is a p_charint_couple
			 */
			if (charint_couple_comparison(a->val,b->val)>0)
			{
				if(a == head) {
					tmp = b -> prox;
					b->prox = a;
					a->prox = tmp;
					head = b;
					c = b;
				} else {
					tmp = b->prox;
					b->prox = a;
					a->prox = tmp;
					c->prox = b;
					c = b;
				}
			} else {
				c = a;
				a = a->prox;
			}
			b = a->prox;
			if(b == e)
				e = a;
		}
	}

	l->first=head;
}


/*
 * list quicksort, custom implementation
 */
void list_quicksort_by_charint_couple(list **l_pointer) {
	list *l=*l_pointer;
	cell *head=l->first;

	if (head==NULL)
		return;

	// if the list is of length 1, it's sorted
	if (head->prox == NULL)
		return;

	p_charint_couple pivot = head->val;

	cell *current_position = head->prox;
	list *less_than_pivot = list_create();
	list *more_than_pivot = list_create();

	while(current_position != NULL) {
		if (charint_couple_comparison(current_position->val,pivot)<=0)
			list_add(less_than_pivot,current_position->val);
		else
			list_add(more_than_pivot,current_position->val);
		current_position=current_position->prox;
	}

	list_quicksort_by_charint_couple(&less_than_pivot);
	list_quicksort_by_charint_couple(&more_than_pivot);

	list *result = list_create();


	cell *less_than_pivot_pos =  less_than_pivot->first;
	while (less_than_pivot_pos != NULL)
	{
		list_add_tail(result,less_than_pivot_pos->val);
		less_than_pivot_pos=less_than_pivot_pos->prox;
	}

	list_add_tail(result,pivot);

	cell *more_than_pivot_pos =  more_than_pivot->first;
	while (more_than_pivot_pos != NULL)
	{
		list_add_tail(result,more_than_pivot_pos->val);
		more_than_pivot_pos=more_than_pivot_pos->prox;
	}

	list_free(less_than_pivot);
	list_free(more_than_pivot);
	list_free(l);
	*l_pointer = result;
}


#ifdef __cplusplus
}
#endif
