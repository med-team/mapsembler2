/**
 * Copyright INRIA and ENS, contributors Peterlongo and Chikhi
 * pierre.peterlongo@inria.fr
 * rayan.chikhi@irisa.fr
 *
 * This software is a computer program whose purpose is to detect the
 * presence of a starter in a  of NGS reads, and to provide the neighbor
 * in case of success..
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

#ifndef READ_GROUPS_H_
#define READ_GROUPS_H_



#include <assert.h>
#include "couple.h"
#include "list.h"
//#include "../commons.h"
#include "hash.h"
//#include "simplehash.h"
#include "read_coherence.h"


typedef struct
{
	int number_substitutions;
	int farthest_position;
	char *consensus;
	int nb_reads;
	int *fragment_coverage;
	list_ *contained_reads;
} group_info;

typedef list_* group_t;

extern int debug_read_coherence;


void create_new_group(group_t *new_group, group_info **current_group_info, const char* fragment);

void free_group(group_t current_read_group, group_info *current_read_group_info);


int append_read_to_group(group_t current_group, group_info *current_group_info, char *current_read, int current_read_len, int position_fragment, const char * fragment, int len_fragment, int current_read_weight, int group_coverage_aware, int group_substitutions_aware, int max_substitutions_allowed, hash_t groups_consensuses);

int append_contained_read_to_group(group_t current_group, group_info *current_group_info, char *current_read, int current_read_len, int position_fragment, const char * fragment, int len_fragment, int current_read_weight, int group_coverage_aware, int group_substitutions_aware, int max_substitutions_allowed, hash_t groups_consensuses);

void increage_group_coverage( group_info *current_group_info, int position_fragment, int current_read_len, int len_fragment);

void copy_group_coverage(group_info *old_current_group_info,group_info *new_current_group_info, int len_fragment);

int append_group_to_groups(int *nb_groups,list_ *read_groups, group_t new_group, group_info *new_group_info);

void remove_group_from_groups(int *nb_groups,list_ *read_groups, cell_mapsembler *group_cell);

void free_read_groups(list_ *read_groups, int also_free_groups);

int find_group(list_ *read_groups, group_t current_group);

int print_group_cover(group_t group, group_info *group_info, char * fragment, signed int left);

#endif /* */
