
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
//#include "read_coherence_mapsembler/couple.h"
#include "commons.h"
#include "../minia/Bank.h"
#include "../minia/Kmer.h"
#include "../minia/Hash16.h"
#include "../minia/Set.h"
#include "../minia/Pool.h"
#include "../minia/Bloom.h"
#include "../minia/Debloom.h"
#include "../minia/Utils.h"
#include "../minia/SortingCount.h"
#include "../minia/Terminator.h"
#include "GraphOutput.h"
#include "IterativeExtensions.h"

#ifndef _FRAGMENT_H
#define _FRAGMENT_H

/**
 * A fragment designes a sequence together with its comment and some coverage information.
 * A fragment stores reads mapped on itself
 **/
class Fragment{
 public: 
  
  /**
   * the initial sequence
   */
  string fragment_sequence; // the fragment to be kmer-coherency-tested
  /**
   * the eventual comment of the fragment (from fasta or fastq file)
   */
  string fragment_comment; // the comment of the fragment to be kmer-coherency-tested
    
    /**
     * vector of left extrem kmers kmers
     */
    vector<string> left_extrem_kmers;
    
    /**
     * vector of right extrem kmers kmers
     */
    vector<string> right_extrem_kmers;
    
  /**
   * Stores the initial sequence
   */
  Fragment(string fragment_sequence);
    bool isPalindromic();
    
    void set_comment(string fragment_comment);
    
    void add_left_extreme_kmer (string left_extrem_kmer);
    
    void add_right_extreme_kmer (string right_extrem_kmer);
    
    void perform_extension(const string prefix, int max_graph_depth, const char extension_type, int max_nodes, parcours_t search_mode);
    
    void fragment_output_graph(GraphOutput graph);
    void fragment_output_sequence(int index, string output_res, bool erase);
    bool is_left_kmer (const int left_extrem_kmer_id);
    bool is_right_kmer (const int right_extrem_kmer_id);
  /**
   * delete the hash table and free the fragment sequence
   */
  ~Fragment(); 

};

#endif //_FRAGMENT_H
