#include <iostream>
#include <vector>
#include "Fragment.h"

#include "commons.h"
#include "../minia/Bank.h"
#include "GraphOutput.h"
using namespace std;
#ifndef _FRAGMENT_BANK_H
#define _FRAGMENT_BANK_H

/**
 * A set of indexible fragments
 */
class Fragment_Bank{
public:
    // The extension types could be
    // 1/ a strict sequence: any branching stops the extension
    // 2/ a consensus sequence: uses minia contiging approach
    // 3/ a strict graph: any branching is conserved in the graph
    // 4/ a consensus graph: "small" polymorphism is merged, but "large" structures are represented.
    char extension_type;
    /**
     * All fragments stored in the set
     */
    vector<Fragment> all_fragments;
    Fragment_Bank(const char type);
    /**
     * Free all fragments, delete the hash table
     */
    ~Fragment_Bank();
    
    /**
     * Read fragments from a file, and load them in the set of fragments
     */
    void load_fragments(char * fragment_file);
    void perform_extensions(const string prefix, int max_graph_depth,int max_nodes, parcours_t search_mode );
    void format_results(const int extension_type, string prefix_name);
    
};

#endif // _FRAGMENT_BANK_H
