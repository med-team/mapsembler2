
# Welcome

This modules performs the first step of Mapsembler2.
For each starter, it outputs a list of "substaters" consisting of extremities of the starter that are found in the reads, up to 1 indel or mismatch


the algorithm goes as follows:

for each starter;
    for each kmer u at the extremity of this starter
        for each k-mer u' where u' is u with an indel or a subsitution or just u
            see how many times u' is in the reads
            if u' is present more than M times (M=3 by default)
                then output u' as a extremal substarter for this starter

             


# Project build

For building your project, you should do the following
    
    mkdir build;  cd build;  cmake ..;  make
    
Then, you should get a binary holding the name of the project.

Note: the first compilation should take some time since the GATB-CORE library is generated.



