# Check availability of C++ TR1 contents.
#
# Sets the following variables:
#
# SHARED_PTR_FOUND          -- std::tr1::shared_ptr1<T> available
# SHARED_PTR_USE_TR1_MEMORY -- #include <tr1/memory>
# SHARED_PTR_USE_MEMORY     -- #include <memory>
# We need to have at least this version to support the VERSION_LESS argument to 'if' (2.6.2) and unset (2.6.3)
cmake_policy(PUSH)
cmake_minimum_required(VERSION 2.6.3)
cmake_policy(POP)
# ---------------------------------------------------------------------------
# std::tr1::shared_ptr<T>
# ---------------------------------------------------------------------------
check_cxx_source_compiles(
    "
        #include <tr1/memory>
        int main() {
            std::tr1::shared_ptr<int> ptr;
            return 0;
        }
    "
    SHARED_PTR_USE_TR1_MEMORY)
check_cxx_source_compiles(
    "
        #include <memory>
        int main() {
            std::shared_ptr<int> ptr;
            return 0;
        }
    "
    SHARED_PTR_USE_MEMORY)
set (SHARED_PTR -NOTFOUND)
if (SHARED_PTR_USE_TR1_MEMORY)
  set (SHARED_PTR_FOUND TRUE)
endif (SHARED_PTR_USE_TR1_MEMORY)
if (SHARED_PTR_USE_MEMORY)
  set (SHARED_PTR_FOUND TRUE)
endif (SHARED_PTR_USE_MEMORY)
mark_as_advanced (SHARED_PTR_FOUND)
mark_as_advanced (SHARED_PTR_USE_TR1_MEMORY)
mark_as_advanced (SHARED_PTR_USE_MEMORY)
# ---------------------------------------------------------------------------
# std::tr1::unordered_map<K, V>
# ---------------------------------------------------------------------------
check_cxx_source_compiles(
    "
        #include <tr1/unordered_map>
        int main() {
            std::tr1::unordered_map<int, int> m;
            return 0;
        }
    "
    UNORDERED_MAP_USE_TR1_UNORDERED_MAP)
check_cxx_source_compiles(
    "
        #include <unordered_map>
        int main() {
            std::unordered_map<int, int> m;
            return 0;
        }
    "
    UNORDERED_MAP_USE_UNORDERED_MAP)
set (UNORDERED_MAP -NOTFOUND)
if (UNORDERED_MAP_USE_TR1_UNORDERED_MAP)
  set (UNORDERED_MAP_FOUND TRUE)
endif (UNORDERED_MAP_USE_TR1_UNORDERED_MAP)
if (UNORDERED_MAP_USE_UNORDERED_MAP)
  set (UNORDERED_MAP_FOUND TRUE)
endif (UNORDERED_MAP_USE_UNORDERED_MAP)
mark_as_advanced (UNORDERED_MAP_FOUND)
mark_as_advanced (UNORDERED_MAP_USE_TR1_UNORDERED_MAP)
mark_as_advanced (UNORDERED_MAP_USE_UNORDERED_MAP)