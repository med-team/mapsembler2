--------------------------------------------------------------------------------
# RELEASE 1.0.5

Modifications of Kmer::Model class for kmers management
* better implementation (factorization, optimization)
* introduction of minimizers concept

WARNING ! These modifications introduced small API changes. Please read snippets kmer2 and kmer5 to see how to handle kmers now. 